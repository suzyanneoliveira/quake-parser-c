#include<stdio.h>
#include<string.h>
#include<stdlib.h>

//estrutura de dados para quardar as informa��es de cado vez que inicar um novo jogo
struct game{
    int total_kills;    //armazena o total de kills que houve durante um jogo
    char players[1000]; //armazena os nome dos jogadores de um jogo
};


int main(){
    FILE *entrada;                  //usado para armazenar o arquivo games.log
    char leitor;                    //usado para auxiliar na leitura do arquivo
    char linha[200];                //usado para ler uma linha do arquivo
    int aux, i=0;                   //usado para auxiliar nas opera��es com matriz e vetores
    int cont_game=0;                //usado para contar a quantidade de games
    int cont_tam_nome=0;            //usado para contar o tamanho do nome de um
    int cont_jogadores=0;           //usado para contar a quantidade de jogadores
    char *point_game, *point_kill, *point_player, *point_killed, *point_kill_world, *compara; //usado para marcar a posi��es

    struct game game[50];           //cria um vetor de estruturas

    char matriz_j[20][15];          //matriz para armazenar os jogadores por jogo
    int matriz_k[30];               //vetor para armazenar os kills de jogadores

    for(i=0;i<30;i++){              //inicializa a matriz com 0
        matriz_k[i]=0;
    }

    entrada = fopen("games.log", "r"); //abre arquivo

    if(entrada != 0){
        while((leitor = getc(entrada) ) != EOF){ //ler aquivo ate o final
            fgets(linha,50,entrada);       //ler uma linha do arquivo

            //procura novo jogo
            point_game= strstr(linha,"InitGame");     //verifica se nessa linha inicia um novo jogo
            if(point_game){                         // se verdadeiro
                printf("\n\ncomecou novo jogo GAME%d\n", cont_game);
                printf("os jogadores desse game sao: %s\n", game[cont_game].players);
                printf("este game teve %d kills\n", game[cont_game].total_kills);
                for(i=0; i<cont_jogadores;i++){
                    printf("O jogador %s matou %d vezes.\n",matriz_j[i], matriz_k[i]); //imprime os jogadores e seus kills
                }
                cont_game++;                        //contador de jogos
                game[cont_game].total_kills=0;      //reinicia  o contador
                cont_jogadores=0;
            }

            //procura jogador
            point_player=strstr(linha,"ClientUserinfoChanged");
            if(point_player){
                if(point_player=strstr(linha,"n\\")){       // se encontrar 'n\' encontrou o inicio nodo nome
                    cont_tam_nome=0;                        // conta o tamanho do nome
                    int posi_jogador= (point_player-linha)+2;   //posi��o do inicio do nome
                    int i=0;
                    int inicio=posi_jogador;                //posi��o do inicio do nome para auxiliar na captura do tamanho do nome
                    while(linha[inicio]!= '\\'){            //procura o final do nome '\'
                        cont_tam_nome++;                    //armazena o tamanho do nome
                        inicio++;
                    }
                    char nome_jogador[cont_tam_nome];       //variavel para armazenar o tamanho do nome
                    while(linha[posi_jogador]!= '\\'){
                        nome_jogador[i]=linha[posi_jogador];//guarda o nome numa variavel
                        i++;
                        posi_jogador++;
                    }

                    //inserir na struct
                    if(strstr(game[cont_game].players,nome_jogador)< 1){    //se o nome n�o existir na struct guarda
                        strcat(game[cont_game].players, ", ");              //concatena em uma string
                        strcat(game[cont_game].players, nome_jogador);

                        //coloca os jogadores em uma matriz
                        strcpy(matriz_j[cont_jogadores], nome_jogador);
                        cont_jogadores++;
                    }
                }
            }

            //conta kills de cada jogador
            point_kill= strstr(linha, "Kill:");             //procura uma linha que houve morte
            if(point_kill){
                game[cont_game].total_kills++;                  //conta a quantidade de kills por game
                if(point_kill_world=strstr(linha, "<world>")){ //se o mundo matou kill--
                    game[cont_game].total_kills--;
                }

                int posi_jogador= (point_kill-linha)+13;    //posi��o do inicio do nome
                int i=0;
                point_killed=strstr(linha, "killed");       //procura killed
                int posi_killed = (point_killed-linha) -1;  //posi_killed vai guardar a posi��o final do nome de quem matou
                cont_tam_nome=posi_killed - posi_jogador;   //calcula o tamanho do nome
                char nome_jogador[cont_tam_nome];               //define o tamanho do nome do jogador
                int tam_matriz=1;
                if(linha[posi_killed-1]!= '>' ){                // se encontrar diferente de '>' � porque n�o foi o mundo que matou
                    while(posi_jogador < posi_killed){
                        nome_jogador[i]=linha[posi_jogador];    //guarda o nome numa variavel
                        i++;
                        posi_jogador++;
                    }

                    //insere os kills de um jogador em uma matriz
                    aux=0;
                    while(aux<cont_jogadores){
                        compara=strstr(matriz_j[aux],nome_jogador); //se o jogador ja existir incrementa um kill pra ele
                        if(compara){
                            matriz_k[aux]=matriz_k[aux] + 1;
                        }
                        aux++;
                    }
                }
            }
        }

    }else{
        printf("falhou\n");
        system("pause");
    }
    fclose(entrada);
}
